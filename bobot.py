import re
import requests
import time
from notion.client import NotionClient
from notion.block import TodoBlock, TextBlock


project_id = "21641722"
token_v2 = "9fe95c00d374c61af8efa3bcd1f69ee563833df8ef58e84c09cc422f9aa8866ce05d49cc9614c23c33cce07dbd8e5f220a2695a16cd2558033174f24ba69c4f07a2e8be0e44f764be6e9413a090d"
page_url = "https://www.notion.so/Test-notion-py-2f3e30b4221c489190aa30710b72b6cc"

endpoint = "https://gitlab.com/api/v4/projects/"+project_id+"/repository/commits"
client = NotionClient(token_v2=token_v2)
page = client.get_block(page_url)

def update(response):
    for child in page.children:
            child.remove()

    for commit in response.json():
        page.children.add_new(TextBlock, title="["+commit["committed_date"]+"] "+commit["message"])

response = requests.get(endpoint)
response_json = response.json()
last_commit_id = response_json[0]["id"]
print("last commit id:", last_commit_id)
update(response=response)

while True:

    response = requests.get(endpoint)
    if response.json()[0]["id"] != last_commit_id:

        print("ada commit baru!")
        last_commit_id = response.json()[0]["id"]
        print("new commit id:", last_commit_id)
        print("new commit message:", last_commit_id)
        
        update(response)

        time.sleep(30)
    else:
        # print("skipped")
        time.sleep(30)


import requests
import time
from notion.client import NotionClient
from notion.block import TodoBlock, TextBlock


project_id = "32599668"
token_v2 = "9fe95c00d374c61af8efa3bcd1f69ee563833df8ef58e84c09cc422f9aa8866ce05d49cc9614c23c33cce07dbd8e5f220a2695a16cd2558033174f24ba69c4f07a2e8be0e44f764be6e9413a090d"
page_url = "https://www.notion.so/notion-gitlab-commits-27ac576609554a4f9250062c0155c093"

endpoint = "https://gitlab.com/api/v4/projects/"+project_id+"/repository/commits"
client = NotionClient(token_v2=token_v2)
page = client.get_block(page_url)

def update(response):
    for child in page.children:
            child.remove()

    for commit in response.json():
        page.children.add_new(TextBlock, title="["+commit["committed_date"]+"] "+commit["message"])

response = requests.get(endpoint)
update(response)
from os import remove
from notion.client import NotionClient
from notion.block import TodoBlock, TextBlock

# Obtain the `token_v2` value by inspecting your browser cookies on a logged-in (non-guest) session on Notion.so
client = NotionClient(token_v2="9fe95c00d374c61af8efa3bcd1f69ee563833df8ef58e84c09cc422f9aa8866ce05d49cc9614c23c33cce07dbd8e5f220a2695a16cd2558033174f24ba69c4f07a2e8be0e44f764be6e9413a090d")

# Replace this URL with the URL of the page you want to edit
page = client.get_block("https://www.notion.so/Test-notion-py-2f3e30b4221c489190aa30710b72b6cc")

print("The old title is:", page.title)

# Note: You can use Markdown! We convert on-the-fly to Notion's internal formatted text data structure.
page.title = "Ini buat Test notion-py"

for child in page.children:
    print(child.title)

print("Parent of {} is {}".format(page.id, page.parent.id))

newchild = page.children.add_new(TodoBlock, title="Something to get done")
newchild.checked = True

page.children.add_new(TextBlock, title="Help me plz")


for child in page.children:
    # print(child.id)
    child.title = "langsung ganti aja bosss"